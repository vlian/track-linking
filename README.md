# track linking

debugging of Aki's tracking.cpp algorithm

# Compile tracking

Set ROOTSYS and FEDRA_ROOT directories, then compile tracking executable:

cl tracking.cpp -nologo -EHsc -I%ROOTSYS%\include -I%FEDRA_ROOT%\include /link -LIBPATH:%ROOTSYS%\lib -LIBPATH:%FEDRA_ROOT%\lib libCore.lib libRint.lib libEve.lib libPhysics.lib libMathCore.lib libTree.lib libRIO.lib libThread.lib libEbase.lib libEdr.lib libvt.lib libEphys.lib libEIO.lib libEmath.lib libEdb.lib

# Reproduce John's error

Run on John's data set:

tracking.exe lnk.def


# NOTE:

Both tracking.cpp and trackingst.cpp run in single-threaded mode, but tracking.cpp also defines MT methods and sets the number of threads to 8. Using trackingst.cpp to be sure there is no multithreading.

# Expected result:

The number of tracks identified will change every time ``tracking.exe lnk.def`` is executed.
